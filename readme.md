# ForkIo

### Tech stack
* Responsive.
  * S: 320px - 480px
  * M: 480px - 992px
  * L: over 992px 
* HTML/JS
* SaSS
* Gulp
* Browser-sync

### Demo
Live demo is hosted [Here](https://kit-portfolio.gitlab.io/flexible-layouts/forkio/).

### Launch
To run the project follow the next steps:
* Install node modules
* Run `gulp build`
* Run `gulp dev`
  * Be advised: sometimes images minification takes time, so if images are not in place, just wait till the minification process is finished (message in CLI will appear, usually takes 10 seconds) and refresh the page.
* Enjoy

# Preview
![Project preview](preview.png#center)